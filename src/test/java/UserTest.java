import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

public class UserTest {
    User user;
    @Before
    public void setUp() {
        user = new User("yamin",  "123", "000-0001", "yamin@gmail.com", 97899034);
    }

    @Test
    public void shouldCreateNewUser(){
        assertThat(user, Is.is(notNullValue()));
    }
}
