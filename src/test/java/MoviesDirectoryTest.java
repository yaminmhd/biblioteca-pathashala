import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;

public class MoviesDirectoryTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private MoviesDirectory moviesDirectory;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        moviesDirectory = new MoviesDirectory();
    }
    @Test
    public void shouldAddMoviesInAList() {
        Movie movie = new Movie("Dark Knight Rises", 2012, "Christopher Nolan", "8");

        moviesDirectory.addMovie(movie);

        assertThat(4, is(equalTo(moviesDirectory.movieListSize())));
    }

    @Test
    public void shouldBeAbleToCheckoutAvailableMovie() {
        moviesDirectory.checkoutMovie(1);

        assertEquals(2, moviesDirectory.movieListSize());
    }

    @Test
    public void shouldNotBeAbleToCheckoutMovieIfNotAvailable() {
        moviesDirectory.retrieveAvailableMovie(1).markItemAsBorrowed();

        moviesDirectory.checkoutMovie(1);

        assertEquals("That movie is not available\n", outContent.toString());
    }

    @Test
    public void shouldNotBeAbleToCheckoutMovieWhenProvidedInvalidInput() {
        moviesDirectory.checkoutMovie(5);

        assertEquals("Movie id does not exist! Try again with another id\n", outContent.toString());
    }
}
