import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class BooksDirectoryTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private BooksDirectory booksDirectory;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        booksDirectory = new BooksDirectory();
    }

    @Test
    public void shouldAddBooksInAList() {
        Book book = new Book("Pragmatic Programmer", "Andy Hunt", 1999);

        booksDirectory.addBook(book);

        assertThat(5, is(equalTo(booksDirectory.bookListSize())));
    }

    @Test
    public void shouldBeAbleToCheckoutAvailableBook() {
        booksDirectory.checkoutBook(1);

        assertFalse(booksDirectory.retrieveCheckedOutBook(1).isItemAvailable());
        assertEquals(3, booksDirectory.bookListSize());
        assertEquals(1, booksDirectory.checkedOutBookListSize());
    }

    @Test
    public void shouldNotBeAbleToCheckoutBookIfNotAvailable() {
        booksDirectory.retrieveAvailableBook(1).markItemAsBorrowed();

        booksDirectory.checkoutBook(1);

        assertEquals("That book is not available\n", outContent.toString());
    }

    @Test
    public void shouldNotBeAbleToCheckoutBookWhenProvidedInvalidInput() {
        booksDirectory.checkoutBook(5);

        assertEquals("Book id does not exist! Try again with another id\n", outContent.toString());
    }

    @Test
    public void shouldBeAbleToReturnCheckedOutBook() {
        booksDirectory.checkoutBook(1);

        booksDirectory.returnBook(1);

        assertEquals(4, booksDirectory.bookListSize());
        assertTrue(booksDirectory.retrieveAvailableBook(1).isItemAvailable());
        assertThat(outContent.toString(), containsString("Thank you for returning the book"));
    }

    @Test
    public void shouldNotBeAbleToReturnCheckoutBookIfIdIsOutOfIndex() {
        booksDirectory.checkoutBook(1);

        booksDirectory.returnBook(0);

        assertThat(outContent.toString(), containsString("That is not a valid book to return"));
    }

    @Test
    public void shouldNotBeAbleToReturnCheckoutBookIfIdIsAboveTheSizeOfList() {
        booksDirectory.checkoutBook(1);

        booksDirectory.returnBook(5);

        assertThat(outContent.toString(), containsString("That is not a valid book to return"));
    }
}
