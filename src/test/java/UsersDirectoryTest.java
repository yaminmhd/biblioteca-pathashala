import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class UsersDirectoryTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private UsersDirectory usersDirectory;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        usersDirectory = new UsersDirectory();
    }
    @Test
    public void shouldAddUserInAList() {
        User user = new User("yamin",  "123", "000-0001", "yamin@gmail.com", 97899034);
        usersDirectory.addUser(user);

        assertThat(4, is(equalTo(usersDirectory.usersListSize())));
    }
}
