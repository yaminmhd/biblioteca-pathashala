import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class BibliotecaTest {
    private IO mockIO;
    private Biblioteca bibliotecaApp;
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void beforeEach() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
        mockIO = mock(IO.class);
        bibliotecaApp = new Biblioteca(mockIO);
    }

    @Test
    public void expectWelcomeMessageToBePrinted() {
        bibliotecaApp.welcome();

        verify(mockIO).display("Welcome to Biblioteca Library System");
    }

    @Test
    public void expectSayHelloWhenCalledWithName() {
        when(mockIO.getInput()).thenReturn("Hello");
        String name = bibliotecaApp.sayHiToUser();

        assertEquals("Hello", name);
    }

    @Test
    public void expectShowMenuMessageToBeDisplayed() {
        String menu = "\nChoose a menu option\n\n" +
                "1 - List Books\n" +
                "2 - Checkout Book\n" +
                "3 - Return Book\n" +
                "4 - List Movies\n" +
                "5 - Checkout Movie\n" +
                "6 - Display Current User Details\n" +
                "7 - Login\n" +
                "q - Quit\n";
        bibliotecaApp.menuOptions();

        verify(mockIO, only()).display(menu);
    }

    @Test
    public void expectApplicationToQuitWhenQuitCommandIsCalled() {
        when(mockIO.getInput()).thenReturn("q");

        bibliotecaApp.menuOptions();

        clearInvocations(mockIO);
        verifyNoMoreInteractions(mockIO);
    }

    @Test
    public void expectToShowListOfBooksWhenListBookIsCalled() {
        String expectedResult = "********************Book List********************\n" +
                "Id    Name                 Author               Year Published \n" +
                "1     Harry Potter         J.K Rowling          1999           \n" +
                "\n" +
                "2     Lord Of The Rings    Peter Jackson        2001           \n" +
                "\n" +
                "3     Game Of Thrones      George R.R. Martin   2007           \n" +
                "\n" +
                "4     Head First Java      Kathy Sierra         2010           \n";
        bibliotecaApp.selectMenuOptionWithChoice("1");
        assertThat(outContent.toString(), containsString(expectedResult));
    }

    @Test
    public void expectToShowListOfMoviesWhenListMovieIsCalled() {
        String expectedResult = "********************Movie List********************\n" +
                "Id    Name                 Year       Director             Rating         \n" +
                "1     Dark Knight Rises    2012       Christopher Nolan    8              \n" +
                "\n" +
                "2     Fast and Furious     2006       Justin Lin           -              \n" +
                "\n" +
                "3     Fight Club           1999       David Fincher        7              \n";
        bibliotecaApp.selectMenuOptionWithChoice("4");
        assertThat(outContent.toString(), containsString(expectedResult));
    }

    @Test
    public void expectInvalidMessageWhenInvalidInputGiven(){
        String expectedResult = "=====================\n" +
                "Select a valid option!\n" +
                "=====================";
        bibliotecaApp.selectMenuOptionWithChoice("XYZ");
        assertThat(outContent.toString(), containsString(expectedResult));
    }
}
