import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class MovieTest {
    @Test
    public void movieShouldBeUnavailableOnceCheckedOut(){
        MoviesDirectory movieList = new MoviesDirectory();
        Movie availableMovie = movieList.retrieveAvailableMovie(1);

        movieList.checkoutMovie(1);

        assertFalse(availableMovie.isItemAvailable());
    }

    @Test
    public void movieShouldHaveARating(){
        Movie movieWithRating = new Movie("testMovie", 2001, "testDirector", "5");

        assertEquals("5", movieWithRating.retrieveRating());
    }

    @Test
    public void movieShouldNotHaveARating(){
        Movie movieWithRating = new Movie("testMovie", 2001, "testDirector");

        assertEquals("-", movieWithRating.retrieveRating());
    }
}
