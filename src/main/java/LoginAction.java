public class LoginAction<T> implements Actions {

    private T directory;
    private IO io;

    LoginAction(T directory, IO io) {
        this.directory = directory;
        this.io = io;
    }

    private T get() {
        return directory;
    }

    @Override
    public void performAction() {
        if(get() instanceof UsersDirectory){
            UsersDirectory usersDirectory = (UsersDirectory) get();

            if(usersDirectory.checkAnyUsersLoggedIn()){
                usersDirectory.logout(usersDirectory.currentLoggedInUser);
            }else{
                usersDirectory.login(getInputForLoginCredentials());
            }
        }
    }

    private String[] getInputForLoginCredentials() {
        String[] loginCredentialsArray = new String[2];
        io.display("Library Number: ");
        String libraryNumber = io.getInput();
        io.display("Password: ");
        String password = io.getInput();
        loginCredentialsArray[0] = libraryNumber;
        loginCredentialsArray[1] = password;
        return loginCredentialsArray;
    }
}
