//A library management system
class Biblioteca {

    private final IO io;
    private ActionMapper actionMapper;
    private boolean isRunning = true;

    Biblioteca(IO io) {
        this.io = io;
        BooksDirectory booksDirectory = new BooksDirectory(io);
        MoviesDirectory moviesDirectory = new MoviesDirectory(io);
        UsersDirectory usersDirectory = new UsersDirectory(io);
        this.actionMapper = new ActionMapper(io, booksDirectory, moviesDirectory, usersDirectory);
    }

    void run() {
        String choice;
        while(isRunning){
            menuOptions();
            choice = io.getInput();
            selectMenuOptionWithChoice(choice);
        }
    }

    void welcome() {
        io.display("Welcome to Biblioteca Library System");
    }

    void menuOptions() {
        String menu = "\nChoose a menu option\n\n" +
                "1 - List Books\n" +
                "2 - Checkout Book\n" +
                "3 - Return Book\n" +
                "4 - List Movies\n" +
                "5 - Checkout Movie\n" +
                "6 - Display Current User Details\n" +
                actionMapper.checkStatus() +
                "q - Quit\n";
        io.display(menu);
    }

    void selectMenuOptionWithChoice(String choice) {
        if(choice.equals("q")){
            this.isRunning = false;
            return;
        }
        actionMapper.perform(choice);
    }

    String sayHiToUser() {
        return io.getInput();
    }
}
