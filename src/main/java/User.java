public class User{
    private String username;
    private String password;
    private String libraryNumber;
    private String emailAddress;
    private int phoneNumber;
    private boolean isLoggedIn = false;

    public User(String username, String password, String libraryNumber, String emailAddress, int phoneNumber) {
        this.username = username;
        this.password = password;
        this.libraryNumber = libraryNumber;
        this.emailAddress = emailAddress;
        this.phoneNumber = phoneNumber;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public boolean checkCredentials(String libraryNumber, String password){
        if(this.libraryNumber.equals(libraryNumber) && this.password.equals(password)){
            return true;
        }
        return false;
    }

    public void markAsLoggedOut() {
        isLoggedIn = false;
    }

    public void markAsLoggedIn() {
        this.isLoggedIn = true;
    }

    public void printDetails() {
        String titleTemplate = "%-10s %-15s %-20s %-15s%n";
        System.out.println("********************User Details********************");
        System.out.printf(titleTemplate, "User Name", "Library Number", "Email Address", "Phone Number");
        System.out.println(String.format("%-10s %-15s %-20s %-15s%n", username, libraryNumber, emailAddress, phoneNumber));
    }
}
