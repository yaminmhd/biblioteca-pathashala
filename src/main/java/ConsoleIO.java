import java.util.List;
import java.util.Scanner;

//A parser to handle the application input and output
public class ConsoleIO implements IO {
    private Scanner scanner;

    ConsoleIO(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public void display(String message) {
        System.out.println(message);
    }

    @Override
    public void displayBookList(List<Book> list) {
        String titleTemplate = "%-5s %-20s %-20s %-15s%n";
        System.out.println("********************Book List********************");
        System.out.printf(titleTemplate, "Id", "Name", "Author", "Year Published");
        int index = 1;
        for (Book book : list) {
            book.printDetails(index);
            index++;
        }
    }

    @Override
    public void displayMovieList(List<Movie> list) {
        String titleTemplate = "%-5s %-20s %-10s %-20s %-15s%n";
        System.out.println("********************Movie List********************");
        System.out.printf(titleTemplate, "Id", "Name", "Year", "Director", "Rating");
        int index = 1;
        for (Movie movie : list) {
            movie.printDetails(index);
            index++;
        }
    }

    @Override
    public String getInput() {
        return scanner.nextLine().toLowerCase();
    }
}

