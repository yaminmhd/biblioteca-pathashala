interface LibraryItem {
    void printDetails(int index);

    boolean isItemAvailable();

    void markItemAsBorrowed();

    void markItemAsAvailable();
}
