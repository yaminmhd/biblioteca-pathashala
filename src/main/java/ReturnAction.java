import java.util.Scanner;
//Returning of items to respective directories
public class ReturnAction<T> implements Actions {
    private IO io = new ConsoleIO(new Scanner(System.in));
    private T directory;

    ReturnAction(T directory) {
        this.directory = directory;
    }

    private T get() {
        return directory;
    }

    @Override
    public void performAction() {
        if (get() instanceof BooksDirectory) {
            BooksDirectory booksDirectory = (BooksDirectory) get();
            if (booksDirectory.verifyCheckedOutBookListSizeIsZero()) {
                io.display("You have not checked out any books!");
                return;
            }
            booksDirectory.displayBookListDetails("checkedOut");
            io.display("Enter the id of the book you plan to return");
            try {
                booksDirectory.returnBook(Integer.valueOf(io.getInput()));
            } catch (NumberFormatException exception) {
                io.display("You have entered an invalid option! Try again");
            }
        }
    }
}
