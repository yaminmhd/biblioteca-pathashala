import java.util.ArrayList;
import java.util.List;

public class UsersDirectory {
    private List<User> usersList = new ArrayList<>();
    User currentLoggedInUser;
    private IO io;

    UsersDirectory(IO io){
        this.io = io;
        addUser(new User("Yamin",  "123", "000-0001", "yamin@gmail.com", 97899034));
        addUser(new User("King", "1234", "000-0002", "king@gmail.com", 12332313));
        addUser(new User("Kong", "12345", "000-0003", "kong@gmail.com", 89761234));
    }

    void addUser(User user) {
        usersList.add(user);
    }

    boolean checkAnyUsersLoggedIn(){
        boolean result = false;
        for(User user: usersList){
            if(user.isLoggedIn()){
                result = true;
            }
        }
        return result;
    }

    void login(String[] loginCredentials){
        String libraryNumber = loginCredentials[0];
        String password = loginCredentials[1];
        if(authenticateUser(libraryNumber, password ) != null){
            io.display("You have login successfully");
        }else{
            io.display("Your credentials are wrong. Please try again!");
        }
    }

    private User authenticateUser(String libraryNumber, String password){
        for(User user: usersList){
            if(user.checkCredentials(libraryNumber, password)){
                user.markAsLoggedIn();
                currentLoggedInUser = user;
                return user;
            }
        }
        return null;
    }

    public void logout(User currentLoggedInUser) {
        currentLoggedInUser.markAsLoggedOut();
        io.display("You have logged out of the system!");
    }

    public int usersListSize() {
        return usersList.size();
    }

    public void displayCurrentUserDetails(User currentLoggedInUser) {
        currentLoggedInUser.printDetails();
    }
}