import java.util.List;

public interface IO {
    void display(String message);

    void displayBookList(List<Book> list);

    void displayMovieList(List<Movie> list);

    String getInput();
}
