import java.util.HashMap;
import java.util.Map;

//Controller for mapping user's choice to the respective actions
class ActionMapper {
    private UsersDirectory usersDirectory;
    private IO io;
    private static final String DISPLAY_BOOKS = "1";
    private static final String CHECKOUT_BOOKS = "2";
    private static final String RETURN_BOOKS = "3";
    private static final String DISPLAY_MOVIES = "4";
    private static final String CHECKOUT_MOVIES = "5";
    private static final String DISPLAY_CURRENT_USER_DETAILS = "6";
    private static final String LOGIN = "7";

    private static Map<String, Actions> strategyMapper = new HashMap<>();

    public ActionMapper(IO io, BooksDirectory booksDirectory, MoviesDirectory moviesDirectory, UsersDirectory usersDirectory) {
        this.io = io;
        this.usersDirectory = usersDirectory;
        strategyMapper.put(DISPLAY_BOOKS, new DisplayAction<>(booksDirectory, "available"));
        strategyMapper.put(CHECKOUT_BOOKS, new AuthorisedAction(new CheckoutAction<>(booksDirectory), usersDirectory, io));
        strategyMapper.put(RETURN_BOOKS, new AuthorisedAction(new ReturnAction<>(booksDirectory), usersDirectory, io));
        strategyMapper.put(DISPLAY_MOVIES, new DisplayAction<>(moviesDirectory));
        strategyMapper.put(CHECKOUT_MOVIES, new AuthorisedAction(new CheckoutAction<>(moviesDirectory), usersDirectory, io));
        strategyMapper.put(DISPLAY_CURRENT_USER_DETAILS, new AuthorisedAction(new DisplayAction<>(usersDirectory), usersDirectory, io));
        strategyMapper.put(LOGIN, new LoginAction<>(usersDirectory, io));
    }

    String checkStatus() {
        return usersDirectory.checkAnyUsersLoggedIn() ? "7 - Logout\n" : "7 - Login\n";
    }

    void perform(String choice) {
        try {
            strategyMapper.get(choice).performAction();
        } catch (Exception e) {
            io.display("=====================\n" +
                    "Select a valid option!\n" +
                    "=====================");
        }
    }
}
