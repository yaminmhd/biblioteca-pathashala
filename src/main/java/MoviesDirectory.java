import java.util.ArrayList;

//Manages the directory of all movies in the library
public class MoviesDirectory {
    private ArrayList<Movie> movieList = new ArrayList<>();
    private IO io;

    MoviesDirectory(IO io){
        this.io = io;
        addMovie(new Movie("Dark Knight Rises", 2012, "Christopher Nolan", "8"));
        addMovie(new Movie("Fast and Furious", 2006, "Justin Lin"));
        addMovie(new Movie("Fight Club", 1999, "David Fincher", "7"));
    }

    void displayMovieListDetails() {
        io.displayMovieList(movieList);
    }

    void addMovie(Movie movie){
        movieList.add(movie);
    }

    int movieListSize() {
        return movieList.size();
    }

    public void checkoutMovie(int index) {
        Movie movie;
        if (index <= 0 || index > movieList.size()) {
            io.display("Movie id does not exist! Try again with another id");
            return;
        }
        movie = retrieveAvailableMovie(index);
        if (!movie.isItemAvailable()) {
            io.display("That movie is not available");
        } else {
            movie.markItemAsBorrowed();
            movieList.remove(movie);
            io.display("Thank you! Enjoy the Movie");
        }
    }

    Movie retrieveAvailableMovie(int index) {
        return movieList.get(index - 1);
    }
}
