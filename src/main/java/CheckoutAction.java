import java.util.Scanner;

//Checking out of items from respective directories
public class CheckoutAction<T> implements Actions {
    private IO io = new ConsoleIO(new Scanner(System.in));
    private T directory;

    CheckoutAction(T directory) {
        this.directory = directory;
    }

    private T get() {
        return directory;
    }

    @Override
    public void performAction() {
        if(get() instanceof BooksDirectory){
            BooksDirectory booksDirectory = (BooksDirectory) get();
            booksDirectory.displayBookListDetails("available");
            io.display("Enter the id of the book you plan to checkout");
            try {
                booksDirectory.checkoutBook(Integer.valueOf(io.getInput()));
            } catch (NumberFormatException exception) {
                io.display("You have entered an invalid option! Try again");
            }
        }

        if(get() instanceof MoviesDirectory){
            MoviesDirectory moviesDirectory = (MoviesDirectory) get();
            moviesDirectory.displayMovieListDetails();
            io.display("Enter the id of the movie you plan to checkout");
            try {
                moviesDirectory.checkoutMovie(Integer.valueOf(io.getInput()));
            } catch (NumberFormatException exception) {
                io.display("You have entered an invalid option! Try again");
            }
        }
    }
}
