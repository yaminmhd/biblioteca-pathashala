//Represent individual book state
class Book implements LibraryItem {
    private String name;
    private String author;
    private int yearPublished;
    private boolean isBookAvailable = true;

    Book(String name, String author, int yearPublished) {
        this.name = name;
        this.author = author;
        this.yearPublished = yearPublished;
    }

    @Override
    public void printDetails(int index) {
        System.out.println(String.format("%-5s %-20s %-20s %-15s%n", index, name, author, yearPublished));
    }

    @Override
    public boolean isItemAvailable() {
        return isBookAvailable;
    }

    @Override
    public void markItemAsBorrowed() {
        this.isBookAvailable = false;
    }

    @Override
    public void markItemAsAvailable() {
        this.isBookAvailable = true;
    }
}
