//Actions that are authorized by the system for logged in users
public class AuthorisedAction implements Actions {
    private Actions primaryAction;
    private UsersDirectory usersDirectory;
    private IO io;

    public AuthorisedAction(Actions directory, UsersDirectory usersDirectory, IO io) {
        primaryAction = directory;
        this.usersDirectory = usersDirectory;
        this.io = io;
    }

    @Override
    public void performAction() {
        if(usersDirectory.checkAnyUsersLoggedIn()){
            primaryAction.performAction();
        }else{
            io.display("Please login first");
        }
    }
}
