//Manages movie's state
public class Movie implements LibraryItem {
    private String name;
    private int year;
    private String director;
    private boolean isMovieAvailable = true;
    private String rating = "-";

    public Movie(String name, int year, String director) {
        this.name = name;
        this.year = year;
        this.director = director;
    }

    public Movie(String name, int year, String director, String rating) {
        this.name = name;
        this.year = year;
        this.director = director;
        evaluateRating(rating);
    }

    private void evaluateRating(String newRating) {
        int convertedRating = Integer.valueOf(newRating);
        if (convertedRating > 0 && convertedRating <= 10) {
            this.rating = newRating;
        }
    }

    @Override
    public void printDetails(int index) {
        System.out.println(String.format("%-5s %-20s %-10s %-20s %-15s%n", index, name, year, director, rating));
    }

    @Override
    public boolean isItemAvailable() {
        return isMovieAvailable;
    }

    @Override
    public void markItemAsBorrowed() {
        this.isMovieAvailable = false;
    }

    @Override
    public void markItemAsAvailable() {
        this.isMovieAvailable = true;
    }

    public String retrieveRating() {
        return this.rating;
    }
}
