//Displaying of items from respective directories
public class DisplayAction<T> implements Actions{
    private T directory;
    private String typeOfList;

    DisplayAction(T directory, String typeOfList) {
        this.directory = directory;
        this.typeOfList = typeOfList;
    }

    DisplayAction(T directory) {
        this.directory = directory;
    }

    private T get() {
        return directory;
    }

    @Override
    public void performAction() {
        if (get() instanceof BooksDirectory) {
            BooksDirectory booksDirectory = (BooksDirectory) get();
            booksDirectory.displayBookListDetails(typeOfList);
        }

        if (get() instanceof MoviesDirectory) {
            MoviesDirectory moviesDirectory = (MoviesDirectory) get();
            moviesDirectory.displayMovieListDetails();
        }

        if (get() instanceof UsersDirectory){
            UsersDirectory usersDirectory = (UsersDirectory) get();
            usersDirectory.displayCurrentUserDetails(usersDirectory.currentLoggedInUser);
        }
    }
}
