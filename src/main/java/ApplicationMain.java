import java.util.Scanner;

//Main application runner
public class ApplicationMain {
    public static void main(String[] args) {
        IO myIO = new ConsoleIO(new Scanner(System.in));
        Biblioteca myApp = new Biblioteca(myIO);
        myApp.welcome();
        myApp.run();
    }
}
