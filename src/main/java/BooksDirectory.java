import java.util.ArrayList;

//Manages the directory of all books in the library
class BooksDirectory {
    private ArrayList<Book> bookList = new ArrayList<>();
    private ArrayList<Book> checkedOutBookList = new ArrayList<>();
    private IO io;

    BooksDirectory(IO io) {
        this.io = io;
        addBook(new Book("Harry Potter", "J.K Rowling", 1999));
        addBook(new Book("Lord Of The Rings", "Peter Jackson", 2001));
        addBook(new Book("Game Of Thrones", "George R.R. Martin", 2007));
        addBook(new Book("Head First Java", "Kathy Sierra", 2010));
    }

    void displayBookListDetails(String typeOfList) {
        if (typeOfList.equals("available")) {
            io.displayBookList(bookList);
        } else {
            io.displayBookList(checkedOutBookList);
        }
    }

    void checkoutBook(int index) {
        Book book;
        if (index <= 0 || index > bookList.size()) {
            io.display("Book id does not exist! Try again with another id");
            return;
        }
        book = retrieveAvailableBook(index);
        if (!book.isItemAvailable()) {
            io.display("That book is not available");
        } else {
            book.markItemAsBorrowed();
            bookList.remove(book);
            addToCheckoutListBook(book);
            io.display("Thank you! Enjoy the book");
        }
    }

    void returnBook(int returnBookIndex) {
        Book book;
        if (returnBookIndex <= 0 || returnBookIndex > checkedOutBookList.size()) {
            io.display("That is not a valid book to return");
            return;
        }

        book = retrieveCheckedOutBook(returnBookIndex);
        book.markItemAsAvailable();
        checkedOutBookList.remove(book);
        bookList.add(book);
        io.display("Thank you for returning the book");
    }

    boolean verifyCheckedOutBookListSizeIsZero() {
        return checkedOutBookListSize() == 0;
    }

    void addBook(Book book) {
        bookList.add(book);
    }

    int bookListSize() {
        return bookList.size();
    }

    int checkedOutBookListSize() {
        return checkedOutBookList.size();
    }

    private void addToCheckoutListBook(Book book) {
        checkedOutBookList.add(book);
    }

    Book retrieveCheckedOutBook(int index) {
        return checkedOutBookList.get(index - 1);
    }

    Book retrieveAvailableBook(int index) {
        return bookList.get(index - 1);
    }
}
